#ifndef WARN_H
#define WARN_H

typedef struct {
    void (*vm_warn)(uint32_t warning_code, uint32_t reg);
} WarnDevice;

inline WarnDevice *warn_init(void (*vm_warn))
{
    WarnDevice *dev;
    dev = mallocz(sizeof(*dev));
    dev->vm_warn = vm_warn;
    return dev;
}

#endif
